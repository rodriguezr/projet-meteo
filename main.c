#include <stdio.h>
#include <stdlib.h>

#include "meteo.h" 

int main(int argc, char *argv[]){
  int* matriceUtile;
  int option, minlong, maxlong, minlat, maxlat, mode, acc=0,cle;
  int ipresent=0;
  char* nomentree;
  char* nomsortie;
  char* mindate;
  char* maxdate;
  tabMeteo* tabtmp;
  matriceUtile = malloc(16 * sizeof(int));
  for(int i;i<argc;i++){
		if(strcmp(argv[i],"-i") == 0){  // on vérifie que le -i soit présent
			ipresent = 1;
		}
	}
  if (ipresent != 1)
  {
    printf("erreur vous n'avez pas préciser le -i. le -i est obligatoire. Plus d'info --help");
    return 1;
  }
  
  // Chaque option possèdera un entier associé
  while((option = getopt(argc,argv,"t:p:whmg:a:d:o:i:H")) != -1 ){
		switch(option){
			case 'i' : 
        nomentree = optarg;
				break;
			case 'h' : //hauteur
				matriceUtile[0]=0;
        acc = acc+1;
        cle = 0;
				break;
			case 'w' : //vent 
				matriceUtile[1] = 1;
        acc = acc+1;
        cle = 1;
				break;
			case 'm' : //humidité 
				matriceUtile[2] = 2;
        acc = acc+1;
        cle = 2;
				break;
			case 'g' : //longitude
				matriceUtile[3] = 3;
        minlong = atoi(optarg);
        option = getopt(argc, argv, "g:");
        if (optind < argc && argv[optind][0] != '-') {
          maxlong = atoi(argv[optind++]); 
        }
        printf("Option longitude: min = %d, max = %d\n", minlong, maxlong);
				break;
			case 'a' : //latitude
        matriceUtile[4] = 4;
        minlat = atoi(optarg);
        if (optind < argc && argv[optind][0] != '-') {
          maxlat = atoi(argv[optind++]); 
        }
        printf("Option latitude: min = %d, max = %d\n", minlat, maxlat);
				break;
			case 'd' : //date
        mindate = optarg;
        if (optind < argc && argv[optind][0] != '-') {
          maxdate = argv[optind++]; 
        }
        printf("Option date: min = %s, max = %s\n", mindate, maxdate);	
				break;
			case 'H' : //help
				matriceUtile[6] = 6;
        cle = 6;     
				break;
			case 'p' : //pression
				matriceUtile[7] = 7;
        mode = atoi(optarg); 
        acc = acc+1;
        cle = 7;
				break;
			case 't' : //température
				matriceUtile[8] = 8;
        mode = atoi(optarg);
        printf("\n%d",mode); 
        acc = acc+1;
        cle = 8;
				break;
			case 'o' : //nom sortie
				nomsortie = optarg;
        printf("%s",nomsortie);
				break;
			default :
				printf("Mettez des options valides, faites ./exe -h ou lisez le README.txt pour connaitre l'ensemble des options");
				break;
		}
	}
  // test des option exclusives
  if (acc > 1)
  {
    printf("trop d'option séléctionnées. Les options -t, -p, -w, -h, et -m sont exclusives.");
    return 1;
  }

  tabtmp = lectureFichier (nomentree); // lecture du fichier CSV donnée
  printf("\n1");  
  if ((matriceUtile[4] == 4)||(matriceUtile[3] == 3)) // trie en priorité la longitude ou la latitude si l'option est activée
  {
    for(int i=0;i<10;i++){
        printf("\n%s %f",tabtmp[i].ID,tabtmp[i].coordonnees[0]);
      }
    tabtmp = longitude(tabtmp,minlong,maxlong);
    for(int i=0;i<10;i++){
        printf("%s %f",tabtmp[i].ID,tabtmp[i].coordonnees[0]);
      }
    tabtmp = lattitude(tabtmp,minlat,maxlat);
    for(int i=0;i<10;i++){
        printf("%s %f",tabtmp[i].ID,tabtmp[i].coordonnees[0]);
      }
     printf("\n2");
  }
  if (matriceUtile[5]==5) // trie en priorité par date si activée
  {
    tabtmp = tridates(tabtmp,mindate,maxdate); 
  }

  // on applique nos fonctions 
	switch(cle){
			case 0 : // hauteur
      printf("\nhauteur");
      for(int i=0;i<10;i++){
        printf("\n%s %f",tabtmp[i].ID,tabtmp[i].coordonnees[0]);
      }
      tabtmp = hauteur(tabtmp);
      printf("\nhauteur");
      for(int i=0;i<10;i++){
        printf("\n%s %f",tabtmp[i].ID,tabtmp[i].coordonnees[0]);
      }
      ecriture(tabtmp,0,nomsortie);
      printf("\nhauteur");
				break;
			case 1 : // vent
      tabtmp = vent(tabtmp);
      ecriture(tabtmp,matriceUtile[1],nomsortie);
      printf("\nvent");
				break;
			case 2 : // humidité
      tabtmp = humiditmax(tabtmp);
      ecriture(tabtmp,matriceUtile[2],nomsortie);
				break;
			case 6 :
      printf("-t <mode> : (t)emperatures.\n");
      printf("-p <mode> : (p)ressions atmosphériques. Pour ces 2 options, il faut indiquer la valeur de <mode> :\n\n ◦ 1 : produit en sortie les températures (ou pressions) minimales, maximales et moyennes par station dans l’ordre croissant du numéro de station.\n\n ◦ 2 : produit en sortie les températures (ou pressions)moyennes par date/heure, triées dans l’ordre chronologique. La moyenne se fait sur toutes les stations. \n\n ◦ 3 : produit en sortie les températures (ou pressions) par date/heure par station. Elles seront triées d’abord par ordre chronologique, puis par ordre croissant de l’identifiant de la station.\n");
      printf("-w : vent ( (w)ind ). Produit en sortie l’orientation moyenne et la vitesse moyenne des vents pour chaque station.\n");
      printf("-h : (h)auteur. Produit en sortie la hauteur pour chaque station. Les hauteurs seront triées par ordre décroissant.\n");
      printf("-m : humidité ( (m)oisture ). Produit en sortie l’humidité maximale pour chaque station. Les valeurs d’humidités seront triées par ordre décroissant.\n");
      printf("-g <min> <max> : lon(g)itude. Permet de filtrer les données de sortie en ne gardant que les données qui sont dans l’intervalle de longitudes [<min>..<max>] incluses. Le format des longitudes est un nombre réel.\n");
      printf("-a <min> <max> : l(a)titude. Permet de filtrer les données de sortie en ne gardant que les relevés qui sont dans l’intervalle de latitudes [<min>..<max>] incluses. Le format des latitudes est un nombre réel.\n");
      printf("-d <min> <max> : (d)ates. Permet de filtrer les données de sortie en ne gardant que les relevés qui sont dans l’intervalle de dates [<min>..<max>] incluses. Le format des dates est une chaine de type YYYY-MM-DD (année-mois-jour).\n");
      printf("-i <nom_fichier> : (f)ichier d’entrée. Permet de spécifier le chemin du fichier CSV d’entrée (fichier fourni). Cette option est obligatoire.\n");
      printf("-o <nom_fichier> : (f)ichier de sortie. Permet de donner un nom au fichier de sortie contenant les données. Si les options utilisées nécessitent plusieurs fichiers, cet argument peut servir à définir le préfixe des fichiers de sortie. Si ce champ n’est pas fourni, la sortie s’appellera <meteoxxxx.dat> par défaut, avec xxxx un nombre sur 4 digits.\n");
				break;					
			case 8 :
        printf("\ntemperature");
        tabtmp = tempe1(tabtmp);
        ecriture(tabtmp,8,nomsortie);
        printf("\n3");
      /*if (mode == 2)
      {
        tabtmp = temperature(tabtmp,mode);
        ecriture(tabtmp,cle,nomsortie);
      }
      if (mode == 3)
      {
        tabtmp = temperature(tabtmp,mode);
        ecriture(tabtmp,cle,nomsortie);
      }*/
				break;
			case 7 :
      if (mode == 1)
      {
        tabtmp = press1(tabtmp);
        ecriture(tabtmp,7,nomsortie);
      }
      /*if (mode == 2)
      {
        tabtmp = pression(tabtmp,mode);
        ecriture(tabtmp,cle,nomsortie);
      }
      if (mode == 3)
      {
        tabtmp = pression(tabtmp,mode);
        ecriture(tabtmp,cle,nomsortie);
      }*/
			break;
		default :
			break;
	}
	
  free(matriceUtile);
  free(tabtmp);
  return 0;
}


