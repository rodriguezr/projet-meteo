exe: meteo.o main.o
	gcc -Wall meteo.o main.o -o exe

meteo.o: meteo.c meteo.h
	gcc -Wall -c meteo.c -o meteo.o

main.o: main.c meteo.h
	gcc -Wall -c main.c -o main.o

clean:
	rm -f *.o
