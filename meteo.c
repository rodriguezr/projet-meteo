
#include "meteo.h"

tabMeteo* lectureFichier(char* nomFichier) {

    FILE* fichier = fopen(nomFichier,"r");
    if (fichier == NULL) {
        printf("Impossible d'ouvrir le fichier %s\n", nomFichier);
        return NULL;
    }

    char ligne[1024];
    char* maLigne;
    int nb_lignes = 0;
    tabMeteo* tableauM = NULL;
    tableauM = malloc(sizeof(tabMeteo)*MAX_LIGNES);

    fgets(ligne,1024,fichier); // On passe la ligne de présentation du csv

    while (fgets(ligne,1024,fichier)) {
        //char* lignePropre= malloc(1024*sizeof(char));
        char* lignevirg = malloc(15*sizeof(char));
        // lignePropre = sansBlanc(ligne,';');
        //printf("%s",lignePropre);
        char* ligne_copy = strdup(ligne);
        maLigne = my_strsep(&ligne_copy,";");
        int nb_colonnes = 0;
        while (maLigne!=NULL) {
            switch (nb_colonnes) {
                case 0:
                    strcpy(tableauM[nb_lignes].ID,maLigne);
                    break;
                case 1:
                    strcpy(tableauM[nb_lignes].date,maLigne);
                    break;
                case 2:
                    tableauM[nb_lignes].pressionm = atof(maLigne);
                    break;
                case 3:
                    tableauM[nb_lignes].dirvent = atoi(maLigne);
                    break;
                case 4:
                    tableauM[nb_lignes].vitvent = atof(maLigne);
                    break;
                case 5:
                    tableauM[nb_lignes].humidite = atoi(maLigne);
                    break;
                case 6:
                    tableauM[nb_lignes].pression_station = atof(maLigne);
                    break;
                case 7:
                    tableauM[nb_lignes].pression_variation = atof(maLigne);
                    break;
                case 8:
                    tableauM[nb_lignes].precipitation = atof(maLigne);
                    break;
                case 9:
                    lignevirg = strtok(maLigne,",");
                    tableauM[nb_lignes].coordonnees[0] = atof(lignevirg);
                    lignevirg = strtok(NULL,",");
                    tableauM[nb_lignes].coordonnees[1] = atof(lignevirg);
                    break;
                case 10:
                    tableauM[nb_lignes].temperature = atof(maLigne);
                    break;
                case 11:
                    tableauM[nb_lignes].temperature_min= atof(maLigne);
                    break;
                case 12:
                    tableauM[nb_lignes].temperature_max = atof(maLigne);
                    break;
                case 13:
                    tableauM[nb_lignes].altitude = atoi(maLigne);
                    break;
                case 14:
                    strcpy(tableauM[nb_lignes].code_postale,maLigne);
                    break;
                default:
                    break;
            }
            maLigne = my_strsep(&ligne_copy,";");
            nb_colonnes++;
        }
        nb_lignes++;
    }
    fclose(fichier);
    return tableauM;
}

tabMeteo* hauteur(tabMeteo* s) {
    int i, j;
    int n = 0;
    tabMeteo* tempo;
    tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    for (i = 0; i < MAX_LIGNES; i++) {
        int trouv_copie;
        trouv_copie = 0;
        for (j = 0; j < n; j++) {
            if (strcmp(s[i].ID, tempo[j].ID) == 0) {
                trouv_copie = 1;
            }
        }
        if (trouv_copie == 0) {
            tempo[n++] = s[i];
        }
    }
    for (i = 0; i < n; i++) {
        for (j = i + 1; j < n; j++) {
            if (tempo[i].altitude < tempo[j].altitude) {
                tabMeteo t;
                t = tempo[i];
                tempo[i] = tempo[j];
                tempo[j] = t;
            }
        }
    } 
    return tempo;
}

tabMeteo* humiditmax(tabMeteo* s){
    int i, j;
    int n;
    n = 0;
    tabMeteo* tempo;
    tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    for (i = 0; i < MAX_LIGNES; i++) {
        int trouv_copie;
        trouv_copie = 0;
        for (j = 0; j < n; j++) {
            if (strcmp(s[i].ID, tempo[j].ID) == 0) {
                trouv_copie = 1;
            }
        }
        if (!trouv_copie) {
            tempo[n++] = s[i];
        }
    }
    for(int i=0;i<n;i++){
        tempo[i].humidite = hummax(s,tempo[i].ID);
    }
    for (i = 0; i < n; i++) {
        for (j = i + 1; j < n; j++) {
            if (tempo[i].humidite < tempo[j].humidite) {
                tabMeteo t;
                t = tempo[i];
                tempo[i] = tempo[j];
                tempo[j] = t;
            }
        }
    }
    printf("%d \n",tempo[61].humidite);
    return tempo;
}

tabMeteo* lattitude(tabMeteo* s, float min, float max){
	int compt=0;
    int lat;
    tabMeteo* nouvListe = NULL; 
	nouvListe = malloc(sizeof(tabMeteo) * MAX_LIGNES);
	for(int i=0;i< MAX_LIGNES;i++){
        lat = s[i].coordonnees[0];
		if((min <= lat)&&(max >= lat)){
			nouvListe[compt] = s[i];
			compt++;
		}
	}
	return nouvListe; // possible modif de copy nvList ds s et return s ?
}

tabMeteo* longitude(tabMeteo* s, float min, float max){
	int compt=0;
    int longi;
    tabMeteo* nouvListe = NULL; 
	nouvListe = malloc(sizeof(tabMeteo) * MAX_LIGNES);
	for(int i=0;i< MAX_LIGNES;i++){
        longi = s[i].coordonnees[1];
		if((min <= longi)&&(max >= longi)){
			nouvListe[compt] = s[i];
			compt++;
		}
	}
	return nouvListe; // possible modif de copy nvList ds s et return s ?
}

tabMeteo* tridates(tabMeteo* tabl, char* datemin, char* datemax) {
    int annee;
    int mois;
    int jour;

    int anneemin;
	int anneemax;
	
	int moismin;
	int moismax;
	
	int jourmin;
	int jourmax;

    int var_bouftou;
    int n;
    n = 0;
    int i;
    float cle_min;
    float cle_max;
    float cle;
    tabMeteo* tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    sscanf(datemin,"%d-%d-%d",&anneemin,&moismin,&jourmin);
    sscanf(datemax,"%d-%d-%d",&anneemax,&moismax,&jourmax);
    cle_min = anneemin*365.25 + moismin * 30.4 + jourmin;
    cle_max = anneemax*365.25 + moismax * 30.4 + jourmax;
    for(i=0;i<MAX_LIGNES;i++){
        sscanf(tabl[i].date, "%d-%d-%dT%d:%d:%d+%d",&annee,&mois,&jour,&var_bouftou,&var_bouftou,&var_bouftou,&var_bouftou);
        cle = annee*365.25 + mois * 30.4 + jour;
        if ((cle >= cle_min)&&(cle <= cle_max)){
            tempo[n] = tabl[i];
            n++;
        }
		
	}
	return tempo; //free les variables//
}


char* my_strsep(char** input, const char* delim) {
    char* start = *input;
    char* p;

    if (start == NULL) {
        return NULL;
    }

    if ((p = strpbrk(start, delim)) != NULL) {
        *p = '\0';
        *input = p + 1;
    } else {
        *input = NULL;
    }

    return start;
}

tabMeteo* vent(tabMeteo* s) {
    int i, j;
    int n = 0;
    tabMeteo* tempo;
    tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    for (i = 0; i < MAX_LIGNES; i++) {
        int trouv_copie;
        trouv_copie = 0;
        for (j = 0; j < n; j++) {
            if (strcmp(s[i].ID, tempo[j].ID) == 0) {
                trouv_copie = 1;
            }
        }
        if (trouv_copie == 0) {
            tempo[n] = s[i]; // faux ?
            n++;
        }
    }
    for (i = 0; i < 63; i++) {
        tempo[i].dirvent = moydirvent(s,tempo[i].ID);
        tempo[i].vitvent = moyvitvent(s,tempo[i].ID);
    }
    for (i = 0; i < 63 - 1; i++) {
        for (j = 0; j <63 - i - 1; j++) {
            if (atoi(tempo[j].ID) > atoi(tempo[j + 1].ID)) {
                tabMeteo temp = s[j];
                s[j] = s[j + 1];
                s[j + 1] = temp;
            }
        }
    }
    return tempo;
}

int moydirvent(tabMeteo* s, char* ID) {
    int i, n = 0;
    int dirventmoy = 0;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0) {
            dirventmoy =dirventmoy + s[i].dirvent;
            n++;
        }
    }
    dirventmoy = dirventmoy / n;
    return dirventmoy;
}

float moyvitvent(tabMeteo* s, char* ID) {
    int i, n = 0;
    float avitventmoy = 0;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0) {
            avitventmoy = avitventmoy + s[i].vitvent;
            n++;
        }
    }
    avitventmoy = avitventmoy / n;
    return avitventmoy;
}

float tempmoy(tabMeteo* s, char* ID) {
    int i, n = 0;
    float tempmoy = 0;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0) {
            tempmoy = tempmoy + s[i].temperature;
            n++;
        }
    }
    tempmoy = tempmoy / n;
    return tempmoy;
}

float tempmin(tabMeteo* s, char* ID) {
    int i = 0;
    float tempmin = 333;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0){
            if (s[i].temperature < tempmin){
                tempmin = s[i].temperature;
            }
        }
    }
    return tempmin;
}

float tempmax(tabMeteo* s, char* ID) {
    int i = 0;
    float tempmax = -999;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0){
            if (s[i].temperature > tempmax){
                tempmax = s[i].temperature;
            }
        }
    }
    return tempmax;
}

float pressmoy(tabMeteo* s, char* ID) {
    int i, n = 0;
    float pressmoy = 0;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0) {
            if(s[i].pression_station > 90000){
                pressmoy = pressmoy + s[i].pression_station;
                n++;
            }
        }
    }
    pressmoy = pressmoy / n;
    return pressmoy;
}

float pressmin(tabMeteo* s, char* ID) {
    int i = 0;
    float pressmin = 1000000;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0){
            if ((s[i].pression_station < pressmin)&&(s[i].pression_station>90000)){
                pressmin = s[i].pression_station;
            }
        }
    }
    return pressmin;
}

float pressmax(tabMeteo* s, char* ID) {
    int i = 0;
    float pressmax = -999;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0){
            if (s[i].pression_station > pressmax){
                pressmax = s[i].pression_station;
            }
        }
    }
    return pressmax;
}


int hummax(tabMeteo* s, char* ID) {
    int i = 0;
    int hummax = -999;

    for (i = 0; i < MAX_LIGNES; i++) {
        if (strcmp(s[i].ID, ID) == 0){
            if (s[i].humidite > hummax){
                hummax = s[i].humidite;
            }
        }
    }
    return hummax;
}

tabMeteo* tempe1(tabMeteo* s) {
    int i, j;
    int n = 0;
    tabMeteo* tempo;
    tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    for (i = 0; i < MAX_LIGNES; i++) {
        int trouv_copie;
        trouv_copie = 0;
        for (j = 0; j < n; j++) {
            if (strcmp(s[i].ID, tempo[j].ID) == 0) {
                trouv_copie = 1;
            }
        }
        if (trouv_copie == 0) {
            tempo[n] = s[i]; // faux ?
            n++;
        }
    }
    for (i = 0; i < 100; i++) {
        tempo[i].temperature = tempmoy(s,tempo[i].ID);
        tempo[i].temperature_min = tempmin(s,tempo[i].ID);
        tempo[i].temperature_max = tempmax(s,tempo[i].ID);
    }
    for (i = 0; i < 61; i++) {
        for (j = 0; j <61 -i -1; j++) {
            if (atoi(tempo[j].ID) > atoi(tempo[j + 1].ID)) {
                tabMeteo temp = tempo[j];
                tempo[j] = tempo[j + 1];
                tempo[j + 1] = temp;
            }
        }
    }
    return tempo;
}

tabMeteo* press1(tabMeteo* s) {
    int i, j;
    int n = 0;
    tabMeteo* tempo;
    tempo = malloc(sizeof(tabMeteo) * MAX_LIGNES);
    for (i = 0; i < MAX_LIGNES; i++) {
        int trouv_copie;
        trouv_copie = 0;
        for (j = 0; j < n; j++) {
            if (strcmp(s[i].ID, tempo[j].ID) == 0) {
                trouv_copie = 1;
            }
        }
        if (trouv_copie == 0) {
            tempo[n] = s[i]; // faux ?
            n++;
        }
    }
    for (i = 0; i < 100; i++) {
        tempo[i].pression_station = pressmoy(s,tempo[i].ID);
        tempo[i].pression_variation = pressmin(s,tempo[i].ID);
        tempo[i].pressionm = pressmax(s,tempo[i].ID);
    }
    for (i = 0; i < 61; i++) {
        for (j = 0; j <61 -i -1; j++) {
            if (atoi(tempo[j].ID) > atoi(tempo[j + 1].ID)) {
                tabMeteo temp = tempo[j];
                tempo[j] = tempo[j + 1];
                tempo[j + 1] = temp;
            }
        }
    }
    return tempo;
}


void ecriture(tabMeteo* MeteoDuBitcoin,int cle,char* nomfich){
	FILE* fichierDonnees;
	fichierDonnees = fopen(nomfich,"w");
	if(cle == 0){
        for(int i=0;i<63;i++){
            if(MeteoDuBitcoin[i].humidite != 0){
			    fprintf(fichierDonnees,"%f %f %d\n",MeteoDuBitcoin[i].coordonnees[0],MeteoDuBitcoin[i].coordonnees[1],MeteoDuBitcoin[i].altitude);
            }	// on écrit les absisses et leur images
		}
	}
    if(cle == 1){
        for(int i=0;i<63;i++){
            if(MeteoDuBitcoin[i].humidite != 0){
			    fprintf(fichierDonnees,"%f %f %d %f\n",MeteoDuBitcoin[i].coordonnees[0],MeteoDuBitcoin[i].coordonnees[1],MeteoDuBitcoin[i].dirvent,MeteoDuBitcoin[i].vitvent);
            }	// on écrit les absisses et leur images
		}
	}
	if(cle == 2){
        for(int i=0;i<63;i++){
            if(MeteoDuBitcoin[i].humidite != 0){
			    fprintf(fichierDonnees,"%f %f %d\n",MeteoDuBitcoin[i].coordonnees[0],MeteoDuBitcoin[i].coordonnees[1],MeteoDuBitcoin[i].humidite);
            }	// on écrit les absisses et leur images
		}
	}
    if(cle == 7){
        for(int i=0;i<63;i++){
            if(MeteoDuBitcoin[i].humidite != 0){
                fprintf(fichierDonnees,"%s %f %f %f\n",MeteoDuBitcoin[i].ID,MeteoDuBitcoin[i].pression_station,MeteoDuBitcoin[i].pression_variation,MeteoDuBitcoin[i].pressionm);
            }	// on écrit les absisses et leur images
		}
	}  
    if(cle == 8){
        for(int i=0;i<63;i++){
            if(MeteoDuBitcoin[i].humidite != 0){
                fprintf(fichierDonnees,"%s %f %f %f\n",MeteoDuBitcoin[i].ID,MeteoDuBitcoin[i].temperature,MeteoDuBitcoin[i].temperature_min,MeteoDuBitcoin[i].temperature_max);
            }	// on écrit les absisses et leur images
		}
	}   
    free(MeteoDuBitcoin);
    fclose(fichierDonnees);
}

