#ifndef __h__
#define __h__
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <getopt.h>
#define M_PI 3.14159265358979323846
#define _USE_MATH_DEFINES
#define MAX_LIGNES 2300000
#define DEFAULTV -999

typedef struct tabMeteo{
    char ID[6];
    char date[22];
    float pressionm;
    int dirvent;
    float vitvent;
    int humidite;
    float pression_station;
    float pression_variation;
    float precipitation;
    float coordonnees[2];
    float temperature;
    float temperature_min;
    float temperature_max;
    int altitude;
    char code_postale[6];
    int n;
}tabMeteo;


/* Auteurs : Pablo BERECOECHEA */
/* Date : 08/12/22 */
/* Résumé : lecture du fichier csv */
/* Entrée(s) : variable qui contient le fichier csv */
/* Sortie(s) : tableau contenant les lignes du fichier csv */
tabMeteo* lectureFichier(char* nomFichier);


/* Auteur : Pablo BERECOECHEA */
/* Date : 01/01/2023 */
/* Résumé : Tri d'un tableau de données de station en fonction de leur humidité */
/* Entrée(s) : tableau de données d'une station (non vide) */
/* Sortie(s) : tableau de données de stations (non vide) trié par ordre décroissant d'humidité */
tabMeteo* humiditmax(tabMeteo* s);


/* Auteur : Pablo BERECOECHEA */
/* Date : 01/01/2023 */
/* Résumé : Tri d'un tableau de données de station en fonction de leur altitude */
/* Entrée(s) : tableau de données de stations (non vide) */
/* Sortie(s) : tableau de données de stations (non vide) trié par ordre croissant d'altitude */
tabMeteo* hauteur(tabMeteo* s);


/* Auteur : Pablo BERECOECHEA */
/* Date : 19/01/2023 */
/* Résumé :   */
/* Entrée(s) :  */
/* Sortie(s) :  */
char* my_strsep(char** stringp, const char* delim);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé : Ecrire les données contenues dans un tableau de données de stations dans un fichier */
/* Entrée(s) : tableau de données d'une station (non vide), un entier et un pointeur de chaine de caractères */
/* Sortie(s) : aucune */
void ecriture(tabMeteo* MeteoDuBitcoin,int cle,char* nomfich);


/* Auteur : Ruben Rodriguez */
/* Date : 22/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction de la direction et de la vitesse moyenne du vent */
/* Entrée(s) : tableau de données d'une station (non vide) */
/* Sortie(s) : tableau de données de stations (non vide) trié par ID croissant de station */
tabMeteo* vent(tabMeteo* s);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé :  Obtenir la direction moyenne du vent d'une station donnée */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : la direction moyenne du vent de la station */
int moydirvent(tabMeteo* s, char* ID);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé : Obtenir la vitesse moyenne du vent d'une station donnée */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : la vitesse moyenne du vent de la station */
float moyvitvent(tabMeteo* s, char* ID);


/* Auteur : Ruben RODRIGUEZ */
/* Date : 19/01/2023 */
/* Résumé :   */
/* Entrée(s) :  */
/* Sortie(s) :  */
tabMeteo* tri(tabMeteo* s,int option);


/* Auteur : Volodia GROMYKHOV */
/* Date : 20/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction d'un intervalle de lattitude */
/* Entrée(s) : tableau de données d'une station (non vide) et deux entiers */
/* Sortie(s) : tableau de données de stations (non vide) trié selon l'intervalle */
tabMeteo* lattitude(tabMeteo* s, float min, float max);


/* Auteur : Volodia GROMYKHOV */
/* Date : 20/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction d'un intervalle de longitude */
/* Entrée(s) : tableau de données d'une station (non vide) et deux entiers */
/* Sortie(s) : tableau de données de stations (non vide) trié selon l'intervalle */
tabMeteo* longitude(tabMeteo* s, float min, float max);


/* Auteur : Pablo BERECOECHEA */
/* Date : 20/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction d'une plage de dates */
/* Entrée(s) : tableau de données d'une station (non vide) et deux pointeurs de chaine de caractères */
/* Sortie(s) : tableau de données de stations (non vide) trié selon la plage de dates */
tabMeteo* tridates(tabMeteo* tabl, char* datemin, char* datemax);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction de la température */
/* Entrée(s) : tableau de données d'une station (non vide) */
/* Sortie(s) : le tableau de données trié */
tabMeteo* tempe1(tabMeteo* s);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé : Obtenir le taux d'humidité maximale d'une station donnée  */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : le taux d'humidité maximale de la station */
int hummax(tabMeteo* s, char* ID);


/* Auteur : Volodia Gromykhov */
/* Date : 22/01/2023 */
/* Résumé : Obtenir la pression maximale d'une station donnée  */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : la pression maximale de la station */
float pressmax(tabMeteo* s, char* ID);


/* Auteur : Volodia Gromykhov */
/* Date : 22/01/2023 */
/* Résumé : Obtenir la pression minimale d'une station donnée */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : la pression minimale de la station */
float pressmin(tabMeteo* s, char* ID);


/* Auteur : Volodia Gromykhov */
/* Date : 22/01/2023 */
/* Résumé : Obtenir la pression moyenne d'une station donnée */
/* Entrée(s) : tableau de données d'une station (non vide) et un pointeur de chaine d'ID */
/* Sortie(s) : un entier : la pression moyenne de la station */
float pressmoy(tabMeteo* s, char* ID);


/* Auteur : Pablo BERECOECHEA */
/* Date : 22/01/2023 */
/* Résumé : Tri d'un tableau de données de stations en fonction de la pression */
/* Entrée(s) : tableau de données d'une station (non vide) */
/* Sortie(s) : le tableau de données trié */
tabMeteo* press1(tabMeteo* s);


#endif
