# Projet Météo 
***
Rodriguez Ruben, Berecoechea Pablo, Gromykhov Volodia
***
Ce programme permet de trier un fichier csv de données météorologiques dans l'objectif d'afficher des graphiques
***
## Table des matières 
***
1. [Informations générales](#informations-générales)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Liste de commandes](#liste-de-commandes)
***
### Informations générales 
***
Le programme comprend cinq fichiers :
 - makefile : permet de compiler/éxecuter le programme
 - main.c : fonction qui marque le début du programme
 - meteo.c : inclu le corps des fonctions et des procédures 
 - meteo.h : inclu les prototypes/signatures des fonctions et des procédures
 - meteo.sh : script schell qui appelle le programme C et qui affiche les graphiques (à l'aide de Gnuplot) en fonction des données triées par le programme C   
	Tous ces diagrammes seront générés par le script schell (quelles que soit les options choisies) :    
		- températures / pressions en mode 1 :    
			diagramme de type barres d’erreur avec en abscisse l’identifiant de la station,   
			et en ordonnée le minimum, maximum et la moyenne.  
 		- températures / pressions en mode 2 :    
			diagramme de type ligne simple avec en abscisse le jour et l’heure des mesures,   
			et en ordonnée les moyenne des mesures.  
		- températures / pressions en mode 3 :    
			diagramme de type multi-lignes avec en abscisse les jours, et en ordonnée les valeurs mesurées.  
			Ce diagramme contiendra toutes les lignes, 1 par station et par heure.  
		- vent :  
			diagramme de type vecteurs (flèches orientées) avec l’abscisse correspondant à la longitude (axe Ouest-Est) et   
			l’ordonnée correspondant à la latitude (axe Nord-Sud).   
		- hauteur :  
			diagramme de type carte interpolée et colorée avec l’abscisse correspondant à la longitude (axe Ouest-Est) et  
			l’ordonnée correspondant à la latitude (axe Nord-Sud).  
		- humidité :  
			diagramme de type carte interpolée et colorée avec l’abscisse correspondant à la longitude (axe Ouest-Est) et   
			l’ordonnée correspondant à la latitude (axe Nord-Sud).  
***
### Technologies 
***
Liste des technologies utilisées pour le projet : 
 - [Gnuplot](http://gnuplot.info/) : Version 5.2
 - [Atom](https://atom.io/) : Version 1.60.0
 - [VisualStudioCode](https://code.visualstudio.com/) : Version 1.74
 - [stdlib.h]
 - [string.h]
 - [stdio.h]
***
### Installation 
***
Vérifier que vous possédez bien les bibliothèques utilisé par le programme.  
Vous pouvez utiliser la commande suivante dans votre terminal pour les installer :  
    _apt install libc6-dev_  
Pour récupérer les fichiers de GitLab, ouvrez votre terminal dans le dossier d'installation, taper la commande suivante dans le terminal :   
    _git clone https://gitlab.etude.cy-tech.fr/rodriguezr/projet-meteo_  
Puis déplacer vous dans le dossier et ouvrez le terminal depuis le dossier.  
La commande de base (pour compiler le programme) à entrer dans le terminal est :   
    _make_   
Puis, effectuer cette commande pour effacer les fichiers inutiles (du type ".o") :  
    _make clean_  
Une fois la compilation terminée, vous pouvez lancer le programme en entrant la commande suivante dans le terminal :   
    _./meteo.sh ...(commandes)..._  
    *(exple : ./meteo.sh -F -i meteo_filtered_data_v1.csv -m)* 
***
### Liste de commandes
***
 - --help : affiche une aide à l’utilisation avec la description de toutes les options possibles ainsi que le rôle qu’elles ont dans l’application.
 - -F : [(F)rance] permet de limiter les mesures à celles présentes en France métropolitaine et en Corse.
 - -G : [(G)uyane] permet de limiter les mesures à celles qui sont présentes en Guyane.
 - -S : [(S)aint-Pierre et Miquelon] permet de limiter les mesures à celles qui sont présentes sur Saint-Pierre et Miquelon.
 - -A : [(A)ntilles] permet de limiter les mesures à celles qui sont présentes aux Antilles.
 - -O : [(O)céan indien] permet de limiter les mesures à celles qui sont présentes dans l’océan indien.
 - -Q : [antarcti(Q)ue] permet de limiter les mesures à celles qui sont présentes en antarctique.
 - -d <min> <max> : [(d)ates] permet de filtrer les dates entre les valeurs min et max. 
