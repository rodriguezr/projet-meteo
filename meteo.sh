#!/bin/bash
#test si l'éxécutable existe
maVar="exe"
if [ ! -x "$maVar" ]; then
    echo "le .exe existe"
else
    echo "le fichier n est pas compiler"
    make clean
    make
    if [ $? -ne 0 ]; then
        echo "erreur de compilation"
        exit 1
    fi
fi
#initialisation des variables 
optF=0
optG=0
optS=0
optA=0
optO=0
optQ=0
optD=0
optH=0
MIN=0
MAX=0
#test des options et attribution des variables avec les options
while [ $# -gt 0 ]; do 
    case $1 in
        -F) optF=1;;
        -G) optG=1;;
        -S) optS=1;;
        -A) optA=1;;
        -O) optO=1;;
        -Q) optQ=1;;
        -d) 
        optD=1 
        MIN="$2"
        MAX="$3"
        ;;
        --help) optH=1;;
        -i) nomfichierEntre="$2";;
        -o) nomfichierSortie="$2";;
        -w) optw=-w;;
        -h) opth=-h;;
        -m) optm=-m;;
        -p) 
        optp=-p
        modep="$2";;
        -t) 
        optt=-t
        modet="$2";;

    esac
    shift
done
diagrammes_barre_erreur() {
    echo "-_-"
    fichierentre="$2"
    fichiersortie="$3"
    gnuplot <<- EOF 
    set datafile separator " "
    set style data histograms
    set style histogram errorbars
    set ylabel "moyenne de $4" 
    set xlabel "identifiant de la station"
    set title "diagrammes barre erreur $1"
    set terminal pdf
    set output "${fichiersortie}.pdf"
    set nokey
    plot "${fichierentre}" using 1:2:3:4 with yerrorbars 
EOF
}
diagrammes_ligne_simple() {
    echo "____"
    fichierentre="$2"
    fichiersortie="$3"
    gnuplot -percist <<- EOF
    set grid
    set datafile separator " "
    set style data lines
    set ylabel "moyenne de $4" 
    set xlabel "jour/heure"
    set xdata time 
    set timefmt "%d\T%H"
    set format x "%d/%H"
    set xtics rotate
    set terminal pdf
    set output "${fichiersortie}.pdf"
    set title "diagrammes ligne simple $1"
    set nokey
    plot "${fichierentre}" using 1:2 with lines notitle 
EOF
}
diagrammes_multi( ) {
    echo "---___"
    fichierentre="$2"
    fichiersortie="$3"
    gnuplot -persist <<- EOF
    set datafile separator " "
    set xdata time
    set timefmt "%Y-%m-%d\T%H"
    set format x "%d"
    set xtics rotate
    set ylabel "Valeurs mesurées"
    set xlabel "Jours"
    et title "Diagramme de type multi-lignes des mesures"
    set terminal pdf
    set output "${fichiersortie}.pdf"
    set key outside
    plot for [i=0:*] "${fichierentre}" using 1:i+2 with lines ls i+1
EOF
}
vecteur() {
    echo "->"
    fichierentre="$2"
    fichiersortie="$3"
    gnuplot -persist <<- EOF
    set grid
    set datafile separator " "
    set xlabel "Longitude (Ouest-Est)"
    set ylabel "Latitude (Nord-Sud)"
    set title "Diagramme de vecteurs des vents de chaque station"
    set key at screen 0.8,0.8
    set terminal pdf
    set output "${fichiersortie}.pdf"
    set nokey
    set title "diagrammes de vecteur $1"
    plot "${fichierentre}" using 2:1:4:3 with vectors head filled lc rgb "red"
EOF
}
carte_interpolee_et_colore3D() {
    echo "carte interpolée et coloré"
    fichierentre="$2"
    fichiersortie="$3"
    gnuplot -persist <<- EOF
    set xyplan relative 0
    set style data pm3d
    set style function pm3d
    set datafile separator " "
    set autoscale x
    set autoscale y
    set autoscale z
    set xlabel "Longitude (Ouest-Est)"
    set xlabel rotate by -8
    set ylabel "Latitude (Nord-Sud)"
    set ylabel rotate by -150
    set zlabel "$4"
    set zlabel rotate
    set palette defined (0 "blue", 100 "green", 200 "yellow", 300 "orange", 400 "red", 800 "black")
    set title "Carte interpolée et colorée des températures $1"
    set dgrid3d
    set terminal pdf
    set output "${fichiersortie}.pdf"
    set nokey
    splot "${fichierentre}" using 2:1:3 with pm3d 
EOF
}
somme=$((optF + optG + optS + optA + optO + optQ + optH))
#vérification si les options -F, -G, -S, -A, -O, et -Q sont exclusives.
if [ $somme -gt 1 ]; then
        echo "trop d'options selecetionnées. les options -F, -G, -S, -A, -O, et -Q sont exclusives."
        exit 1
fi
#vérification des condition de lancement du programme
if [[ $optF -eq 0 && $optG -eq 0 && $optS -eq 0 && $optA -eq 0 && $optO -eq 0 && $optQ -eq 0 && $optD -eq 0 && $optH -eq 0 ]]; then
    echo "il n’y a aucune limite sur la position des relevés."
    ./exe -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet
        if [ "$optw" = -w ]; then
            vecteur "sans limite" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "sans limite" "$nomfichierSortie" cat_hauteur3D$ hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "sans limite" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "sans limite" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "sans limite" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "sans limite" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "sans limite" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
else
    if [ $optF -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données de la France"
        ./exe -a -5 10 -g 40 52 -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet
        if [ "$optw" = -w ]; then
            vecteur "en France" $nomfichierSortie vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "en France" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "en France" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "en France" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "en France" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "en France" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "en France" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
        #diagrammes_multi
    elif [ $optG -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données de la Guyane."
        echo "./exe -a "2\" "6\" -g "-55\" "-51\" -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet"
        if [ "$optw" = -w ]; then
            vecteur "en Guyane" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "en Guyane" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "en Guyane" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "en Guyane" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "en Guyane" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "en Guyane" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "en Guyane" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
        #diagrammes_multi
    elif [ $optS -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données de Saint-Pierre et Miquelon"
        echo "./exe -a "46\" "48\" -g "-57\" "-56\" -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet"
        if [ "$optw" = -w ]; then
            vecteur "à Saint-Pierre et Miquelon" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "à Saint-Pierre et Miquelon" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "à Saint-Pierre et Miquelon" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "à Saint-Pierre et Miquelon" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "à Saint-Pierre et Miquelon" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "à Saint-Pierre et Miquelon" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "à Saint-Pierre et Miquelon" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
        #diagrammes_multi
    elif [ $optA -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données aux Antilles."
        echo "./exe -a "-66\" "-58\" -g "12\" "20\" -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet"
        if [ "$optw" = -w ]; then
            vecteur "aux Antilles" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "aux Antilles" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "aux Antilles" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "aux Antilles" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "aux Antilles" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "aux Antilles" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "aux Antilles" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
        #diagrammes_multi
    elif [ $optO -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données dans l’océan indien."
        echo "./exe -a "-26\" "-11\" -g "41\" "61\" -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet"
        if [ "$optw" = -w ]; then
            vecteur "dans l'océan indien" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "dans l'océan indien" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "dans l'océan indien" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "dans l'océan indien" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "dans l'océan indien" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "dans l'océan indien" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "dans l'océan indien" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi 
        #diagrammes_multi
    elif [ $optQ -eq 1 ]; then
        echo "affichages des diagrammes avec uniquement les données en antarctique."
        echo "./exe -a "-180\" "180\" -g "-90\" "-58\" -i $nomfichierEntre -o $nomfichierSortie $optw $opth $optm $optp $modep $optt $modet"
        if [ "$optw" = -w ]; then
            vecteur "en Antarctique" "$nomfichierSortie" vent
        fi
        if [ "$opth" = -h ]; then
            carte_interpolee_et_colore3D "en Antarctique" "$nomfichierSortie" catre_hauteur3D hauteur
        fi
        if [ "$optm" = -m ]; then
            carte_interpolee_et_colore3D "en Antarctique" "$nomfichierSortie" catre_humidite3D humidité
        fi
        if [ "$modep" = 1 ]; then
            diagrammes_barre_erreur "en Antarctique" "$nomfichierSortie" pression_mode_1 Pression
        fi 
        if [ "$modep" = 2 ]; then
            diagrammes_ligne_simple "en Antarctique" "$nomfichierSortie" pression_mode_2 Pression
        fi
        if [ "$modet" = 1 ]; then
            diagrammes_barre_erreur "en Antarctique" "$nomfichierSortie" temperatures_mode_1 temperatures 
        fi
        if [ "$modet" = 2 ]; then
            diagrammes_ligne_simple "en Antarctique" "$nomfichierSortie" temperatures_mode_2 temperatures
        fi
        #diagrammes_multi
    elif [ $optD -eq 1 ]; then
        echo "filtre les dates a partir de $MIN jusqu'à $MAX"
    elif [ $optH -eq 1 ]; then
        echo " les option du shell :"
        echo "-F : (F)rance 
       permet de limiter les mesures à celles présentes en France métropolitaine + Corse.
◦ -G : (G)uyane
       permet de limiter les mesures à celles qui sont présentes en Guyane.
◦ -S : (S)aint-Pierre et Miquelon 
       permet de limiter les mesures à celles qui sont présentes sur l’ile située à l’Est du Canada.
◦ -A: (A)ntilles
       permet de limiter les mesures à celles qui sont présentes aux Antilles.
◦ -O : (O)céan indien 
       permet de limiter les mesures à celles qui sont présentes dans l’océan indien.
◦ -Q : antarcti(Q)ue 
       permet de limiter les mesures à celles qui sont présentes en antarctique. 
◦ -d <min> <max> : (d)ates 
       permet de filtrer les dates entre les valeurs moin et max, tout comme le ferait le programme C."
       echo "./exe -H"
    fi
fi

